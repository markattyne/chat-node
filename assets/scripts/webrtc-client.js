window.WebRTC = {
	socket: null,
	connect: function() {
		window.WebRTC.socket = io();
		window.WebRTC.socket.on('user-connected', function(data) {
			console.log(data.user + ' connected');
		});
		window.WebRTC.socket.on('user-disconnected', function(data) {
			console.log(data.user + ' disconnected');
		});
		return window.WebRTC;
	},
	subscribe: function(channel) {
		window.WebRTC.socket.emit('subscribe', channel);
		var channelMethods = {};
		channelMethods.bind = function(event, callback) {
			window.WebRTC.bind(channel, event, callback);
			return channelMethods;
		};
		channelMethods.send = function(event, data) {
			window.WebRTC.send(channel, event, data);
		};
		return channelMethods;
	},
	unsubscribe: function(channel) {
		window.WebRTC.socket.emit('unsubscribe', channel);
	},
	bind: function(channel, event, callback) {
		window.WebRTC.socket.on(event, function(data) {
			if (data.room == channel) {
				callback(data);
			}
		});
	},
	send: function(channel, event, data) {
		data.room = channel;
		window.WebRTC.socket.emit(event, data);
	}
};