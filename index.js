var express = require('express');
var app = express();
var http = require('http').Server(app);
var io = require('socket.io')(http);

app.use(express.static(__dirname + '/assets'));

app.get('/', function(req, res) {
	res.sendFile(__dirname + '/views/index.html');
});

io.on('connection', function(socket) {
	var user = socket.conn.id;
	io.emit('user-connected', { user: user });

	socket.on('message', function(data) {
		if (data.room) {
			socket.broadcast.in(data.room).emit('message', { message: data.message, user: user, room: data.room });
		}
		else {
			socket.broadcast.emit('message', { message: data.message, user: user });
		}
	});

	socket.on('disconnect', function() {
		io.emit('user-disconnected', { user: user });
	});

	socket.on('subscribe', function(room) {
		socket.join(room);
		io.sockets.in(room).emit('user-joined', { user: user, room: room });
	});

	socket.on('unsubscribe', function(room) {
		socket.leave(room);
		io.sockets.in(room).emit('user-left', { user: user, room: room });
	});
});

http.listen(3000, function() {
	console.log('listening on *:3000');
});